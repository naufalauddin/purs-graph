{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name = "graph"
, dependencies =
  [ "arrays"
  , "bifunctors"
  , "canvas"
  , "console"
  , "control"
  , "css"
  , "effect"
  , "either"
  , "foldable-traversable"
  , "halogen"
  , "halogen-css"
  , "integers"
  , "lists"
  , "maybe"
  , "numbers"
  , "parsing"
  , "partial"
  , "prelude"
  , "psci-support"
  , "strings"
  , "unfoldable"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
