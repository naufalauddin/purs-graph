module Expr where

import Data.Either (Either)
import Prelude
import Parsing (ParseError, Parser, parseErrorMessage, runParser)
import Parsing.Combinators (many1, (<|>), many)
import Parsing.Expr (Assoc(..), Operator(..), buildExprParser)
import Parsing.String (char, eof, string)
import Parsing.String.Basic (space)

import Control.Lazy (fix)
import Data.Bifunctor (bimap)
import Data.Generic.Rep (class Generic)
import Data.List.NonEmpty (toUnfoldable)
import Data.Maybe (fromMaybe)
import Data.Number (fromString, pow)
import Data.Show.Generic (genericShow)
import Data.String.CodeUnits (fromCharArray)
import Parsing.Token (digit)

data Expr
  = Lit Number
  | Plus Expr Expr
  | Mult Expr Expr
  | Pow Expr Expr
  | Subst Expr Expr
  | Div Expr Expr
  | X

derive instance eqExpr :: Eq Expr
derive instance genericExpr :: Generic Expr _

instance showExpr :: Show Expr where
  show x = genericShow x

eval :: Expr -> Number -> Number
eval (Lit n) = \_ -> n 
eval (Plus a b) = \x -> (eval a x) + (eval b x)
eval (Mult a b) = \x -> (eval a x) * (eval b x)
eval (Pow a b) = \x -> pow (eval a x) (eval b x)
eval (Subst a b) = \x -> (eval a x) - (eval b x)
eval (Div a b) = \x -> (eval a x) / (eval b x)
eval X = \x -> x

parseExpr :: String -> Either ParseError Expr
parseExpr = flip runParser (expr <* eof)

toFunction :: String -> Either String (Number -> Number)
toFunction = bimap parseErrorMessage eval <<< parseExpr

type ExprParser = Parser String Expr

wrappedSpaces :: forall o. Parser String o -> Parser String o
wrappedSpaces parser = many space *> parser <* many space

parens :: forall o. Parser String o -> Parser String o
parens parser = string "(" *> wrappedSpaces parser <* string ")"

expr :: ExprParser
expr = fix allExpr
  where
    allExpr p = buildExprParser table (term p)

table :: forall m. Monad m => Array (Array (Operator m String Expr))
table = [ [ Infix (string "^" $> Pow) AssocRight ]
        , [ Infix (string "*" $> Mult) AssocRight ]
        , [ Infix (string "/" $> Div) AssocRight ]
        , [ Infix (string "+" $> Plus) AssocRight ]
        , [ Infix (string "-" $> Subst) AssocRight ]
        ]

term :: ExprParser -> ExprParser
term p = wrappedSpaces $ parens p <|> lit <|> x_

x_ :: ExprParser
x_ = char 'x' $> X

lit :: ExprParser
lit = (Lit <<< fromMaybe 0.0 <<< fromString <<< fromCharArray <<< toUnfoldable) <$> (many1 digit)
