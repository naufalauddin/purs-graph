module Main where

import Effect.Class (class MonadEffect)
import Prelude

import CSS as CSS
import Data.Array ((..))
import Data.Either (Either(..))
import Data.Int (toNumber, round)
import Data.Maybe (Maybe(..))
import Data.Traversable (for)
import Data.Unfoldable (fromMaybe)
import Effect (Effect)
import Expr (toFunction)
import Graphics.Canvas (Context2D, clearRect, closePath, getCanvasElementById, getContext2D, lineTo, moveTo, scale, setLineDash, setLineWidth, setStrokeStyle, strokePath, translate, withContext)
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.HTML.CSS as HC
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Partial.Unsafe (unsafePartial)

numberOfGrid :: Int
numberOfGrid = 30

space :: Number
space = width / (toNumber numberOfGrid)

width :: Number
width = 600.0

canvasRect :: { height :: Number, width :: Number, x :: Number, y :: Number }
canvasRect = { height: 600.0, width: 600.0, x: 0.0, y: 0.0 }

type Coord = { x :: Number, y :: Number }

line :: Context2D -> Coord -> Coord -> Effect Unit
line ctx from to = withContext ctx do 
  strokePath ctx $ do
    moveTo ctx from.x from.y 
    lineTo ctx to.x to.y 
    closePath ctx

drawAxis :: Context2D -> Effect Unit
drawAxis ctx = withContext ctx do
  strokePath ctx $ do
     moveTo ctx 300.0 0.0
     lineTo ctx 300.0 width
     closePath ctx
  strokePath ctx $ do
     moveTo ctx 0.0 300.0
     lineTo ctx width 300.0
     closePath ctx

yline :: Context2D -> Number -> Effect Unit
yline ctx n = strokePath ctx $ do
  moveTo ctx n 0.0
  lineTo ctx n width
  closePath ctx

xline :: Context2D -> Number -> Effect Unit
xline ctx n = strokePath ctx $ do
  moveTo ctx 0.0   n
  lineTo ctx width n
  closePath ctx

drawGrid :: Context2D -> Effect Unit
drawGrid ctx = void $ withContext ctx do
  setStrokeStyle ctx "#555"
  setLineWidth   ctx 0.3
  setLineDash    ctx [3.0, 5.0]
  for (1 .. numberOfGrid) \n -> do
     xline ctx (toNumber n * space)
     yline ctx (toNumber n * space)

drawFunc :: Context2D -> (Number -> Number) -> Effect Unit
drawFunc ctx f = void $ withContext ctx do
  let f' x = (f (x / space)) * space
      step = 1.0
  translate ctx { translateX: width / 2.0, translateY: width / 2.0 }
  scale ctx { scaleX: 1.0, scaleY: -1.0 }
  setStrokeStyle ctx "#36bdbf"
  setLineWidth ctx 2.0
  for (1 .. (round width)) \n -> do 
    let n' = (toNumber n) - (width / 2.0)
        from = { x: n', y: (f' n') }
        to = { x: (n' + step), y: (f' (n' + step)) }
    line ctx from to

drawCanvas :: Context2D -> (Number -> Number) -> Effect Unit
drawCanvas ctx f = do
  drawGrid ctx
  drawAxis ctx
  drawFunc ctx f

data Action
  = FunctionChange String
  | Initialize

type State = { func :: Number -> Number
             , error :: Maybe String }

component :: forall query input output m. MonadEffect m => H.Component query input output m
component = 
  H.mkComponent 
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval
      { handleAction = handleAction 
      , initialize = Just Initialize                              
      }
    }
  where
    initialState :: forall i. i -> State
    initialState _ = { func: \x -> x, error: Nothing }

    render :: State -> forall m. H.ComponentHTML Action () m
    render s = 
      HH.div
        [ HP.style "display: flex; flex-direction: column" ]
        [ mainStyle
        , HH.canvas [ HP.width (round width), HP.height (round width), HP.id "canvas" ]
        , HH.code [ HP.id "error" ] (fromMaybe $ map HH.text $ s.error)
        , HH.input [ HE.onValueChange (\v -> FunctionChange v)]
        ]
    
    handleAction :: forall o ma. MonadEffect ma => Action -> H.HalogenM State Action () o ma Unit
    handleAction action = case action of 
      FunctionChange f -> do
        case toFunction f of 
          Right f' -> do
            H.liftEffect $ drawOnCanvas f'
            H.modify_ \s -> s { error = Nothing }
          Left error -> H.modify_ \s -> s { error = Just error }


      Initialize -> unsafePartial $ do 
         Just canvas <- H.liftEffect $ getCanvasElementById "canvas"
         ctx <- H.liftEffect $ getContext2D canvas
         H.liftEffect $ drawCanvas ctx (\x -> x)
    
    drawOnCanvas :: (Number -> Number) -> Effect Unit
    drawOnCanvas f = unsafePartial $ do
      Just canvas <- getCanvasElementById "canvas"
      ctx <- getContext2D canvas
      clearRect ctx canvasRect
      drawCanvas ctx f



mainStyle :: forall i o. HH.HTML i o
mainStyle = HC.stylesheet $ do
  CSS.select CSS.star $ CSS.margin (CSS.px 20.0) (CSS.px 10.0) (CSS.px 20.0) (CSS.px 10.0)
  CSS.select (CSS.code CSS.& (CSS.byId "error")) $ CSS.color red

red :: CSS.Color
red = CSS.rgb 255 0 0

main :: Effect Unit
main = HA.runHalogenAff do 
  body <- HA.awaitBody
  runUI component unit body
